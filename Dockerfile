FROM node:16.13.1-alpine3.12
COPY . /app/
# Setup Frontend dependencies
WORKDIR /app
RUN /bin/sh -c 'npm install'
EXPOSE 3000
# Run the app
CMD /bin/sh -c 'npm start'
