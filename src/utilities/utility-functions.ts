/* eslint-disable no-loop-func */
import { TargetType } from "../types/function-params-types"
import { Checklist, ChecklistItem, Task, Comment } from "../types/task-type"

// To sort tasks according to their created and due dates we need the dates to be presented in numbers
export const convertDateToTimestamp = (date: Date): number => {
  return date.valueOf()
}

// An assortment of sorting functions for sorting by dates functionality
export const SortingFunctions = {
  CreatedAscending: (a: Task, b: Task) =>
    convertDateToTimestamp(a.created) - convertDateToTimestamp(b.created),
  CreatedDescending: (a: Task, b: Task) =>
    convertDateToTimestamp(b.created) - convertDateToTimestamp(a.created),
  DueDescending: (a: Task, b: Task) => {
    if (a.due && b.due) {
      return convertDateToTimestamp(a.due) - convertDateToTimestamp(b.due)
    } else if (!a.due) {
      // if a.due === null
      return 1
    } else {
      return -1
    }
  },
  DueAscending: (a: Task, b: Task) => {
    if (a.due && b.due) {
      return convertDateToTimestamp(b.due) - convertDateToTimestamp(a.due)
    } else if (!a.due) {
      // if a.due === null
      return -1
    } else {
      return 1
    }
  }
}

// A reviver function for JSON.parse which selects Date strings and returns Date objects
export const DateReviverAfterJSONStringify = (key: string, value: any) => {
  const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/
  if (typeof value === "string" && dateFormat.test(value)) {
    return new Date(value)
  }
  return value
}

// Checking for id uniqueness
export const getUniqueId = (
  items: Task[] | Checklist[] | ChecklistItem[] | Comment[],
  target: TargetType
) => {
  let Id = String(Date.now())
  switch (target) {
    case "task":
      let TaskWithTheSameId: Task | undefined = undefined
      if (items.length > 0) {
        TaskWithTheSameId = (items as Task[]).find((task) => task._id === Id)
        while (TaskWithTheSameId) {
          Id = String(Date.now())
          TaskWithTheSameId = (items as Task[]).find((task) => task._id === Id)
        }
      }
      return Id
    case "checklist":
      let ChecklistWithTheSameId: Checklist | undefined = undefined
      if (items.length > 0) {
        ChecklistWithTheSameId = (items as Checklist[]).find(
          (checklist) => checklist._id === Id
        )
        while (ChecklistWithTheSameId) {
          Id = String(Date.now())
          ChecklistWithTheSameId = (items as Checklist[]).find(
            (checklist) => checklist._id === Id
          )
        }
      }
      return Id

    case "checklistitem":
      let ChecklistitemWithTheSameId: ChecklistItem | undefined = undefined
      if (items.length > 0) {
        ChecklistitemWithTheSameId = (items as ChecklistItem[]).find(
          (checklistitem) => checklistitem._id === Id
        )
        while (ChecklistitemWithTheSameId) {
          Id = String(Date.now())
          ChecklistitemWithTheSameId = (items as ChecklistItem[]).find(
            (task) => task._id === Id
          )
        }
      }
      return Id
    case "comment":
      let CommentWithTheSameId: Comment | undefined = undefined
      if (items.length > 0) {
        CommentWithTheSameId = (items as Comment[]).find(
          (comment) => comment._id === Id
        )
        while (CommentWithTheSameId) {
          Id = String(Date.now())
          CommentWithTheSameId = (items as Comment[]).find(
            (comment) => comment._id === Id
          )
        }
      }
      return Id
    default:
      return Id
  }
}

export const howManyChecklistItemsAreThereInTheTask = (task: Task) => {
  let counter = 0
  task.checklists.map((checklist) => (counter += checklist.items.length))
  return counter
}

export const howManyChecklistItemsAreCheckedInTheTask = (task: Task) => {
  let counter = 0
  task.checklists.map((checklist) =>
    checklist.items.map((item) => {
      if (item.checked) {
        counter++
      }
      return null
    })
  )
  return counter
}

// Check overdue task
export const isOverdue = (task: Task) => {
  const now = Date.now()
  const due = task.due?.valueOf()
  if (due) {
    return now > due
  } else {
    return false
  }
}
