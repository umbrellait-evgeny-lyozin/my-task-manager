import { createContext } from "react"
import { CurrentTaskContextType } from "../types/context-types"

export const CurrentTaskContext = createContext<CurrentTaskContextType>({
  task: {
    _id: "",
    title: "",
    description: "",
    completed: false,
    created: new Date(),
    due: null,
    checklists: [],
    comments: []
  },
  setTask: () => null
})
