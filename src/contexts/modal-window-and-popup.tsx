import { createContext } from "react"
import { ModalWindowAndPopupContextType } from "../types/context-types"

export const ModalWindowAndPopupContext =
  createContext<ModalWindowAndPopupContextType>({
    toggleModalWindow: () => false,
    setShowConfirmation: () => false,
    setShowPopup: () => false,
    showPopup: {
      show: false,
      text: "",
      type: "null"
    },
    showConfirmation: { show: false, text: "", confirmed: false, id: "" },
    showModal: false
  })
