import { createContext } from "react"
import { ThereIsNewDataContextType } from "../types/context-types"

export const ThereIsNewDataContext = createContext<ThereIsNewDataContextType>({
  thereIsNewData: true,
  setThereIsNewData: () => console.log()
})
