import React, { useState } from "react"
import { ConfirmationPopup } from "./components/generic-components/confirmation-popup"
import { InformationalPopup } from "./components/generic-components/information-popup"
import { ModalWindow } from "./components/modal-window-related-components/modal-window"
import { MainDiv } from "./components/styled-components/styled-divs"
import { TasksBoard } from "./components/tasks-board-related-components/tasks-board"
import { CurrentTaskContext } from "./contexts/current-task"
import { ModalWindowAndPopupContext } from "./contexts/modal-window-and-popup"
import { ThereIsNewDataContext } from "./contexts/there-is-new-data"

import {
  ActionsWhichNeedConfirmation,
  InformationalTextTypes,
  PopupTypes
} from "./types/context-types"
import { Task } from "./types/task-type"
function App() {
  const [showPopup, setShowPopup] = useState<{
    show: boolean
    text: InformationalTextTypes
    type: PopupTypes
  }>({
    show: false,
    text: "",
    type: "null"
  })
  const [showConfirmation, setShowConfirmation] = useState<{
    show: boolean
    text: ActionsWhichNeedConfirmation
    confirmed: boolean
    id: string | string[]
  }>({
    show: false,
    text: "",
    confirmed: false,
    id: ""
  })
  const [showModal, setShowModal] = useState(false)

  const toggleModalWindow = () => {
    setShowModal((prevState) => !prevState)
  }

  const [thereIsNewData, setThereIsNewData] = useState(true)

  const [task, setTask] = useState<Task>({
    _id: "",
    title: "",
    description: "",
    completed: false,
    created: new Date(),
    due: null,
    checklists: [],
    comments: []
  })

  return (
    <ThereIsNewDataContext.Provider
      value={{
        thereIsNewData,
        setThereIsNewData
      }}>
      <CurrentTaskContext.Provider
        value={{
          task,
          setTask
        }}>
        <ModalWindowAndPopupContext.Provider
          value={{
            toggleModalWindow,
            setShowPopup,
            setShowConfirmation,
            showPopup,
            showConfirmation,
            showModal
          }}>
          <MainDiv
            background="white"
            style={showConfirmation.show ? { pointerEvents: "none" } : {}}>
            {showModal ? <ModalWindow /> : <TasksBoard />}
          </MainDiv>
          {showPopup.show ? <InformationalPopup /> : null}
          {showConfirmation.show ? (
            <ConfirmationPopup what={showConfirmation.text} />
          ) : null}
        </ModalWindowAndPopupContext.Provider>
      </CurrentTaskContext.Provider>
    </ThereIsNewDataContext.Provider>
  )
}

export default App
