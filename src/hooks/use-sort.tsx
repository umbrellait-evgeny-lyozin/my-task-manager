import { useCallback } from "react"
import { SortingParams } from "../types/sorting-params"
import { SortingFunctions } from "../utilities/utility-functions"
import { useLocalstorage } from "./use-local-storage"

export const useSort = () => {
  const { Get } = useLocalstorage()

  // Getting sorting functions from utilities

  const { CreatedAscending, CreatedDescending, DueAscending, DueDescending } =
    SortingFunctions

  // The filtering function is needed to filter and sort tasks according to the sort options passed to the hook
  // custom sorting hook is needed so we could apply many sorting options at once and produce single result

  const FilteringAndSortingFunction = useCallback(
    (SortingParams: SortingParams) => {
      const LocalTasks = Get()
      if (LocalTasks.length > 0) {
        let FilteredTasks = LocalTasks.filter((Task) =>
          Task.title.includes(SortingParams.searchString)
        )
        if (SortingParams.showCompleted)
          FilteredTasks = FilteredTasks.filter((Task) => Task.completed)
        switch (SortingParams.sortingOption) {
          case "CreatedDesc":
            FilteredTasks = FilteredTasks.sort(CreatedDescending)
            break
          case "CreatedAsc":
            FilteredTasks = FilteredTasks.sort(CreatedAscending)
            break
          case "DueDesc":
            FilteredTasks = FilteredTasks.sort(DueDescending)
            break
          case "DueAsc":
            FilteredTasks = FilteredTasks.sort(DueAscending)
            break
          default:
            FilteredTasks = FilteredTasks.sort(CreatedDescending)
            break
        }
        return FilteredTasks
      }
    },
    [CreatedAscending, CreatedDescending, DueAscending, DueDescending, Get]
  )

  return { FilteringAndSortingFunction }
}
