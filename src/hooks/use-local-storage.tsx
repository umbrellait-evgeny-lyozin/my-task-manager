import { useCallback, useContext, useState } from "react"
import { ThereIsNewDataContext } from "../contexts/there-is-new-data"
import { Checklist, ChecklistItem, Task, Comment } from "../types/task-type"
import {
  DateReviverAfterJSONStringify,
  getUniqueId
} from "../utilities/utility-functions"
import { TargetType } from "../types/function-params-types"
import { ModalWindowAndPopupContext } from "../contexts/modal-window-and-popup"

export const useLocalstorage = () => {
  const { showConfirmation, setShowConfirmation } = useContext(
    ModalWindowAndPopupContext
  )

  const { setThereIsNewData } = useContext(ThereIsNewDataContext)

  // State so that we can handle components while data is loading
  const [loading, setLoading] = useState(false)
  // State so that we can work with errors if any occured during data loading
  const [error, setError] = useState(null)

  const Get = useCallback((): Task[] => {
    setLoading(true)
    try {
      const TasksLocalData = localStorage.getItem("Tasks")
      setLoading(false)
      return TasksLocalData
        ? JSON.parse(TasksLocalData, DateReviverAfterJSONStringify)
        : []
    } catch (e: any) {
      setLoading(false)
      setError(e.message)
      throw e
    }
  }, [])

  const GetById = useCallback((id: string): Task | undefined => {
    setLoading(true)
    try {
      const TasksLocalData = localStorage.getItem("Tasks")
      const ParsedTasks: Task[] | [] = TasksLocalData
        ? JSON.parse(TasksLocalData, DateReviverAfterJSONStringify)
        : []
      return ParsedTasks.find((task) => task._id === id)
    } catch (e: any) {
      setLoading(false)
      setError(e.message)
      throw e
    }
  }, [])

  const Post = useCallback(
    (title: string): Task => {
      setLoading(true)
      try {
        const TasksLocalData = localStorage.getItem("Tasks")

        const ParsedTasksData: Task[] = TasksLocalData
          ? JSON.parse(TasksLocalData, DateReviverAfterJSONStringify)
          : []

        const Id = getUniqueId(ParsedTasksData, "task")
        const NewTask: Task = {
          _id: Id,
          title,
          description: "",
          completed: false,
          created: new Date(),
          due: null,
          checklists: [],
          comments: []
        }
        const NewTasksArray = [...ParsedTasksData].concat([{ ...NewTask }])
        localStorage.setItem("Tasks", JSON.stringify(NewTasksArray))
        setLoading(false)
        setThereIsNewData(true)
        return NewTask
      } catch (e: any) {
        setLoading(false)
        setError(e.message)
        throw e
      }
    },
    [setThereIsNewData]
  )

  const Delete = useCallback(
    (
      id: string,
      target: TargetType
    ): Task | Checklist | ChecklistItem | Comment | undefined => {
      // If action is confirmed => do it!
      if (!showConfirmation.show && showConfirmation.confirmed) {
        setLoading(true)
        switch (target) {
          case "task":
            try {
              const TasksLocalData = localStorage.getItem("Tasks")
              const ParsedTasksData: Task[] = TasksLocalData
                ? JSON.parse(TasksLocalData, DateReviverAfterJSONStringify)
                : []
              if (ParsedTasksData.length > 0) {
                const DeletedTask = ParsedTasksData.find(
                  (task) => task._id === id
                )
                const RemainingTasks = ParsedTasksData.filter(
                  (task) => task._id !== id
                )
                localStorage.setItem("Tasks", JSON.stringify(RemainingTasks))
                setLoading(false)
                // вернуть состояние подтверждения в первоначальное
                setShowConfirmation({
                  show: false,
                  text: "",
                  confirmed: false,
                  id: ""
                })
                setThereIsNewData(true)
                return DeletedTask as Task
              }
            } catch (e: any) {
              setLoading(false)
              setError(e.message)
              throw e
            }
            break
          case "checklist":
            try {
              const TasksLocalData = localStorage.getItem("Tasks")
              const ParsedTasksData: Task[] = TasksLocalData
                ? JSON.parse(TasksLocalData, DateReviverAfterJSONStringify)
                : []
              if (ParsedTasksData.length > 0) {
                const DeletedChecklist = ParsedTasksData.map((task: Task) =>
                  task.checklists.find((checklist) => checklist._id === id)
                ).find((data) => (data ? data : undefined))
                const RemainingTasks = ParsedTasksData.map((task) => ({
                  ...task,
                  checklists: task.checklists.filter(
                    (checklist) => checklist._id !== id
                  )
                }))
                // Set all tasks in localStorage
                localStorage.setItem("Tasks", JSON.stringify(RemainingTasks))

                setLoading(false)
                // Return confirmation state in previous state
                setShowConfirmation({
                  show: false,
                  text: "",
                  confirmed: false,
                  id: ""
                })
                setThereIsNewData(true)
                return DeletedChecklist as Checklist
              }
            } catch (e: any) {
              setLoading(false)
              setError(e.message)
              throw e
            }
            break
          case "checklistitem":
            try {
              const TasksLocalData = localStorage.getItem("Tasks")
              const ParsedTasksData: Task[] = TasksLocalData
                ? JSON.parse(TasksLocalData, DateReviverAfterJSONStringify)
                : []
              if (ParsedTasksData.length > 0) {
                const DeletedChecklistItem = ParsedTasksData.map((task: Task) =>
                  task.checklists.map((checklist) =>
                    checklist.items.find((item) => item._id === id)
                  )
                )
                  .find((data) => (data ? data : undefined))
                  ?.find((data) => (data ? data : undefined))

                const FilteredTasks = ParsedTasksData.map((task: Task) => ({
                  ...task,
                  checklists: task.checklists.map((checklist) => ({
                    ...checklist,
                    items: checklist.items.filter((item) => item._id !== id)
                  }))
                }))

                // Set all tasks in localStorage
                localStorage.setItem("Tasks", JSON.stringify(FilteredTasks))

                setLoading(false)
                // Return confirmation state in previous state
                setShowConfirmation({
                  show: false,
                  text: "",
                  confirmed: false,
                  id: ""
                })
                setThereIsNewData(true)
                return DeletedChecklistItem as ChecklistItem
              }
            } catch (e: any) {
              setLoading(false)
              setError(e.message)
              throw e
            }
            break
          case "comment":
            try {
              const TasksLocalData = localStorage.getItem("Tasks")
              const ParsedTasksData: Task[] = TasksLocalData
                ? JSON.parse(TasksLocalData, DateReviverAfterJSONStringify)
                : []
              if (ParsedTasksData.length > 0) {
                const DeletedComment = ParsedTasksData.map((task: Task) =>
                  task.comments.find((comment) => comment._id === id)
                ).find((data) => (data ? data : undefined))
                const RemainingTasks = ParsedTasksData.map((task) => ({
                  ...task,
                  comments: task.comments.filter(
                    (comment) => comment._id !== id
                  )
                }))
                // Set all tasks in localStorage
                localStorage.setItem("Tasks", JSON.stringify(RemainingTasks))

                setLoading(false)
                // Return confirmation state in previous state
                setShowConfirmation({
                  show: false,
                  text: "",
                  confirmed: false,
                  id: ""
                })
                setThereIsNewData(true)
                return DeletedComment as Comment
              }
            } catch (e: any) {
              setLoading(false)
              setError(e.message)
              throw e
            }
            break
        }
      }
    },
    [
      setShowConfirmation,
      setThereIsNewData,
      showConfirmation.confirmed,
      showConfirmation.show
    ]
  )

  const Put = useCallback(
    (task: Task): Task | undefined => {
      setLoading(true)
      try {
        const TasksLocalData = localStorage.getItem("Tasks")
        const ParsedTasks: Task[] | [] = TasksLocalData
          ? JSON.parse(TasksLocalData, DateReviverAfterJSONStringify)
          : []
        // To quickly update the task if there is one to be updated
        let UpdatedTaskIndex = -1
        const IsThereTheTaskThatNeedsToBeUpdated =
          ParsedTasks.length > 0
            ? ParsedTasks.find((sometask, index) => {
                UpdatedTaskIndex = index
                return sometask._id === task._id
              })
            : undefined
        if (IsThereTheTaskThatNeedsToBeUpdated) {
          ParsedTasks[UpdatedTaskIndex] = task
        }
        localStorage.setItem("Tasks", JSON.stringify(ParsedTasks))
        setLoading(false)
        setThereIsNewData(true)
        return ParsedTasks[UpdatedTaskIndex] || undefined
      } catch (e: any) {
        setLoading(false)
        setError(e.message)
        throw e
      }
    },
    [setThereIsNewData]
  )
  const clearError = () => setError(null)

  return {
    loading,
    Get,
    GetById,
    Post,
    Delete,
    Put,
    error,
    clearError
  }
}
