import * as yup from "yup"

// Generic error messages
yup.setLocale({
  string: {
    max: (max) => `At most ${max.max} characters is allowed`
  },
  mixed: {
    required: "This field is required"
  }
})

export const TaskTitleSchema = yup
  .object({
    Title: yup.string().max(50).required()
  })
  .required()
export const ChecklistItemsSchema = yup
  .object()
  .shape({
    checklistItems: yup.array().of(
      yup.object().shape({
        label: yup.string().max(50).required()
      })
    )
  })
  .required()

export const CommentInputSchema = yup
  .object({
    body: yup.string().max(300)
  })
  .required()
