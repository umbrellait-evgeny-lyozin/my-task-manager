export type Task = {
  _id: string
  title: string
  description: string
  completed: boolean
  created: Date
  due: Date | null
  checklists: Checklist[] | []
  comments: Comment[] | []
}

export type Checklist = {
  _id: string
  name: string
  items: ChecklistItem[] | []
  completed: boolean
}

export type ChecklistItem = {
  _id: string
  label: string
  checked: boolean
}

export type Comment = {
  _id: string
  body: string
  created: Date
}
