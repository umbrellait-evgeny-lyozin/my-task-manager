// Types for react-hook-form

export type TaskTitleInput = {
  Title: string
}

export type ChecklistItems = {
  checklistItems: {
    label: string
  }[]
}

export type CommentInput = {
  body: string
}
