import { Dispatch, SetStateAction } from "react"
import { Task } from "./task-type"
export type ActionsWhichNeedConfirmation =
  | "delete the task"
  | "delete the checklist"
  | "delete the checklist item"
  | "delete the comment"
  | "delete the tasks"
  | "mark the task as completed"
  | "mark the task as not completed"
  | "mark all selected tasks as completed"
  | ""

export type ModalWindowAndPopupContextType = {
  toggleModalWindow: () => void
  setShowPopup: React.Dispatch<
    React.SetStateAction<{
      show: boolean
      text: InformationalTextTypes
      type: PopupTypes
    }>
  >
  showPopup: {
    show: boolean
    text: InformationalTextTypes
    type: PopupTypes
  }
  showConfirmation: {
    show: boolean
    text: ActionsWhichNeedConfirmation
    confirmed: boolean
    id: string | string[]
  }
  setShowConfirmation: React.Dispatch<
    React.SetStateAction<{
      show: boolean
      text: ActionsWhichNeedConfirmation
      confirmed: boolean
      id: string | string[]
    }>
  >
  showModal: boolean
}

export type ThereIsNewDataContextType = {
  thereIsNewData: boolean
  setThereIsNewData: Dispatch<SetStateAction<boolean>>
}

export type CurrentTaskContextType = {
  task: Task
  setTask: Dispatch<SetStateAction<Task>>
}

export type InformationalTextTypes =
  | "Trying to save a blank comment? Nope."
  | "New task successfully created."
  | "New checklist successfully created."
  | "A task was successfully deleted."
  | ""
  | "The tasks were successfully deleted."
  | "The checklist was successfully deleted."
  | "The comment was added."
  | "The comment was deleted."
  | "The comment was updated."
  | "The checklist items were successfully added."
  | "You have to pick a date first =-)"
export type PopupTypes = "info" | "alert" | "success" | "failure" | "null"

export enum PopupColors {
  info = "#469DA9",
  alert = "#469DA9",
  success = "#46A94A",
  failure = "#A94646",
  null = ""
}

export enum LayoutColors {
  danger = "#A94646",
  create = "#46A94A",
  cancel = "#595D85",
  misc = "#595D85",
  main = "#595D85",
  countermain = "#D4E4DB",
  completed = "#A0C89F",
  shadow = "rgba(89, 93, 133, 0.5)",
  overdue = "#e9a95a",
  white = "white"
}

export enum StandardButtonHeights {
  thick = "5vw",
  thin = "2.5vw"
}
