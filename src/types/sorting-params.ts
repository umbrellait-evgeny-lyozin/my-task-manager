export type SortingParams = {
  searchString: string
  showCompleted: boolean
  sortingOption: string
}
