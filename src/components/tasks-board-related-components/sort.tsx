import React from "react"
import { SortingParams } from "../../types/sorting-params"
import { StyledSortDiv } from "../styled-components/styled-divs"
import { ShowOnlyCompleted } from "./show-only-completed"
import { SortBy } from "./sort-by"

export const Sort = ({
  setSortingParams
}: {
  setSortingParams: React.Dispatch<React.SetStateAction<SortingParams>>
}) => {
  return (
    <StyledSortDiv>
      <SortBy setSortingParams={setSortingParams} />
      <ShowOnlyCompleted setSortingParams={setSortingParams} />
    </StyledSortDiv>
  )
}
