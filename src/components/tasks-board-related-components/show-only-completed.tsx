import React, { useContext, useEffect, useState } from "react"
import { ThereIsNewDataContext } from "../../contexts/there-is-new-data"
import { SortingParams } from "../../types/sorting-params"
import {
  StyledCheckbox,
  StyledCompleteLabel
} from "../styled-components/styled-inputs"

export const ShowOnlyCompleted = ({
  setSortingParams
}: {
  setSortingParams: React.Dispatch<React.SetStateAction<SortingParams>>
}) => {
  const { setThereIsNewData } = useContext(ThereIsNewDataContext)

  const [showOnlyCompleted, setShowOnlyCompleted] = useState(false)

  const taskCompletedCheckboxHandler = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setShowOnlyCompleted(e.target.checked)
  }

  // to update sorting params to show completed or all tasks
  useEffect(() => {
    setSortingParams((params) => ({
      ...params,
      showCompleted: showOnlyCompleted
    }))
    setThereIsNewData(true)
  }, [setSortingParams, setThereIsNewData, showOnlyCompleted])

  return (
    <>
      <StyledCompleteLabel htmlFor="taskCompleted">
        Completed
      </StyledCompleteLabel>
      <StyledCheckbox
        type="checkbox"
        name="taskCompleted"
        checked={showOnlyCompleted}
        onChange={taskCompletedCheckboxHandler}
      />
    </>
  )
}
