import React, { useContext, useEffect, useState } from "react"
import { ThereIsNewDataContext } from "../../contexts/there-is-new-data"
import { SortingParams } from "../../types/sorting-params"
import { StyledInput } from "../styled-components/styled-inputs"

export const SearchByTitle = ({
  setSortingParams
}: {
  setSortingParams: React.Dispatch<React.SetStateAction<SortingParams>>
}) => {
  const { setThereIsNewData } = useContext(ThereIsNewDataContext)

  const [searchString, setSearchString] = useState("")

  const filterTasks = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchString(e.target.value)
  }

  // to add new searching string to sorting params
  useEffect(() => {
    setSortingParams((params) => ({
      ...params,
      searchString: searchString
    }))
    setThereIsNewData(true)
  }, [searchString, setSortingParams, setThereIsNewData])

  return (
    <StyledInput
      name="searchBar"
      type="text"
      placeholder="Search"
      onChange={filterTasks}
      value={searchString}
    />
  )
}
