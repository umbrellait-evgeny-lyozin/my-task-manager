import React, { useContext, useEffect, useState } from "react"
import { ThereIsNewDataContext } from "../../contexts/there-is-new-data"
import { SortingParams } from "../../types/sorting-params"
import {
  StyledSelect,
  StyledSortLabel
} from "../styled-components/styled-inputs"

export const SortBy = ({
  setSortingParams
}: {
  setSortingParams: React.Dispatch<React.SetStateAction<SortingParams>>
}) => {
  const { setThereIsNewData } = useContext(ThereIsNewDataContext)

  const [sortingOption, setSortingOption] = useState("CreatedDesc")

  const sortingHandler = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSortingOption(e.target.value)
  }

  // to update sorting params to show tasks according to a specific sorting order
  useEffect(() => {
    setSortingParams((params) => ({
      ...params,
      sortingOption: sortingOption
    }))
    setThereIsNewData(true)
  }, [setSortingParams, setThereIsNewData, sortingOption])
  return (
    <>
      <StyledSortLabel htmlFor="sort">Sort by</StyledSortLabel>
      <StyledSelect onChange={sortingHandler} name="sort" value={sortingOption}>
        <option value="CreatedDesc">Most recent</option>
        <option value="CreatedAsc">Oldest</option>
        <option value="DueDesc">Most urgent</option>
        <option value="DueAsc">Less urgent</option>
      </StyledSelect>
    </>
  )
}
