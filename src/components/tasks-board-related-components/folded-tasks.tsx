import React, { useContext, useEffect, useState } from "react"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import { useLocalstorage } from "../../hooks/use-local-storage"
import { LayoutColors } from "../../types/context-types"
import { Task } from "../../types/task-type"
import { StyledButton } from "../styled-components/styled-buttons"
import { ButtonsDiv, StyledTasksDiv } from "../styled-components/styled-divs"
import { FoldedTask } from "./folded-task"

export const FoldedTasks = ({ Tasks }: { Tasks: Task[] }) => {
  const { setShowPopup } = useContext(ModalWindowAndPopupContext)
  const { showConfirmation, setShowConfirmation } = useContext(
    ModalWindowAndPopupContext
  )

  const { Delete, GetById, Put } = useLocalstorage()

  // Select many functionality
  const [selectMany, setSelectMany] = useState(false)
  const [selectedIds, setSelectedIds] = useState<string[]>([])

  const cancelSelection = () => {
    setSelectMany(false)
    setSelectedIds([])
  }

  // Delete tasks functionality

  useEffect(() => {
    if (!showConfirmation.show && showConfirmation.text === "delete the task") {
      Delete(showConfirmation.id as string, "task")
      setShowConfirmation({
        show: false,
        text: "",
        confirmed: false,
        id: ""
      })
      setShowPopup({
        show: true,
        text: "A task was successfully deleted.",
        type: "alert"
      })
    }
  }, [
    Delete,
    setShowConfirmation,
    setShowPopup,
    showConfirmation.id,
    showConfirmation.show,
    showConfirmation.text
  ])

  // Delete many tasks at once
  const deleteTasks = () => {
    setShowConfirmation({
      show: true,
      text: "delete the tasks",
      confirmed: false,
      id: selectedIds
    })
  }

  useEffect(() => {
    if (
      !showConfirmation.show &&
      showConfirmation.text === "delete the tasks"
    ) {
      for (const id of showConfirmation.id as string[]) {
        Delete(id, "task")
      }
      setShowConfirmation({
        show: false,
        text: "",
        confirmed: false,
        id: ""
      })
      cancelSelection()

      setShowPopup({
        show: true,
        text: "The tasks were successfully deleted.",
        type: "alert"
      })
    }
  }, [
    Delete,
    setShowConfirmation,
    setShowPopup,
    showConfirmation.id,
    showConfirmation.show,
    showConfirmation.text
  ])

  // Many tasks are done

  const markTasksAsDone = () => {
    setShowConfirmation({
      show: true,
      text: "mark all selected tasks as completed",
      confirmed: false,
      id: selectedIds
    })
  }

  useEffect(() => {
    if (
      !showConfirmation.show &&
      showConfirmation.text === "mark all selected tasks as completed"
    ) {
      for (const id of showConfirmation.id as string[]) {
        const UpdatedTask = GetById(id)
        if (UpdatedTask) {
          UpdatedTask.completed = true
          UpdatedTask.checklists = [
            ...UpdatedTask.checklists.map((checklist) => ({
              ...checklist,
              completed: true
            }))
          ]
          for (const checklist of UpdatedTask.checklists) {
            if (checklist.items.length > 0) {
              for (const item of checklist.items) {
                item.checked = true
              }
            }
          }
        }
        // Updating task in localStorage
        Put(UpdatedTask as Task)
      }
      setShowConfirmation({
        show: false,
        text: "",
        confirmed: false,
        id: ""
      })
      cancelSelection()
    }
  }, [
    GetById,
    Put,
    setShowConfirmation,
    showConfirmation.id,
    showConfirmation.show,
    showConfirmation.text
  ])

  return (
    <StyledTasksDiv>
      <ButtonsDiv>
        {selectMany ? (
          <StyledButton
            background={LayoutColors.countermain}
            color={LayoutColors.main}
            onClick={cancelSelection}>
            Cancel
          </StyledButton>
        ) : Tasks.length > 0 ? (
          <StyledButton
            background={LayoutColors.countermain}
            color={LayoutColors.main}
            onClick={() => setSelectMany(true)}>
            Select many
          </StyledButton>
        ) : null}
        {selectedIds.length > 0 ? (
          <>
            <StyledButton
              background={LayoutColors.danger}
              color={LayoutColors.countermain}
              onClick={deleteTasks}>
              Delete
            </StyledButton>
            <StyledButton
              background={LayoutColors.create}
              color={LayoutColors.countermain}
              onClick={markTasksAsDone}>
              Mark as Done
            </StyledButton>
          </>
        ) : null}
      </ButtonsDiv>
      {Tasks.map((task) => (
        <FoldedTask
          task={task}
          key={task._id}
          selectManyFunctionality={{
            selectMany,
            setSelectedIds
          }}
        />
      ))}
    </StyledTasksDiv>
  )
}
