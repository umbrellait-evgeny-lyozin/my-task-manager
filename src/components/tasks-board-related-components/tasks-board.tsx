import React, { useContext, useEffect, useState } from "react"
import { TasksBoardDiv } from "../styled-components/styled-divs"
import { FoldedTasks } from "./folded-tasks"
import { SearchByTitle } from "./search-by-title"
import { Sort } from "./sort"

import { Task } from "../../types/task-type"
import { useSort } from "../../hooks/use-sort"
import { AddTaskForm } from "../forms/add-task-form"
import { ThereIsNewDataContext } from "../../contexts/there-is-new-data"
import { LayoutColors } from "../../types/context-types"

export const TasksBoard = () => {
  const { thereIsNewData, setThereIsNewData } = useContext(
    ThereIsNewDataContext
  )

  const { FilteringAndSortingFunction } = useSort()

  const [Tasks, setTasks] = useState<Task[]>([])

  const [sortingParams, setSortingParams] = useState({
    searchString: "",
    showCompleted: false,
    sortingOption: "CreatedDesc"
  })

  // to update currently shown tasks if there is new data in LocalStorage
  useEffect(() => {
    if (thereIsNewData) {
      const SortedTasks = FilteringAndSortingFunction(sortingParams)
      if (SortedTasks) setTasks(SortedTasks)
      else setTasks([])
      setThereIsNewData(false)
    }
  }, [
    FilteringAndSortingFunction,
    setThereIsNewData,
    sortingParams,
    sortingParams.searchString,
    sortingParams.showCompleted,
    sortingParams.sortingOption,
    thereIsNewData
  ])

  return (
    <TasksBoardDiv background={LayoutColors.main}>
      <SearchByTitle setSortingParams={setSortingParams} />
      <Sort setSortingParams={setSortingParams} />
      <AddTaskForm />
      <FoldedTasks Tasks={Tasks} />
    </TasksBoardDiv>
  )
}
