import React, { useContext, useEffect } from "react"
import { CurrentTaskContext } from "../../contexts/current-task"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import { ThereIsNewDataContext } from "../../contexts/there-is-new-data"
import { useLocalstorage } from "../../hooks/use-local-storage"
import { LayoutColors } from "../../types/context-types"
import { Task } from "../../types/task-type"
import {
  howManyChecklistItemsAreCheckedInTheTask,
  howManyChecklistItemsAreThereInTheTask,
  isOverdue
} from "../../utilities/utility-functions"
import { StyledButton } from "../styled-components/styled-buttons"
import { StyledTaskDiv } from "../styled-components/styled-divs"
import {
  StyledCheckedIcon,
  StyledCommentsIcon,
  StyledDeleteIcon,
  StyledEditIcon,
  StyledInfoIcon
} from "../styled-components/styled-icons"
import { StyledDarkCheckbox } from "../styled-components/styled-inputs"
import {
  StyledCheckedCounter,
  StyledCommentsCounter,
  StyledTaskTitle
} from "../styled-components/styled-text-related-elements"
export const FoldedTask = ({
  task,
  selectManyFunctionality
}: {
  task: Task
  selectManyFunctionality: {
    selectMany: boolean
    setSelectedIds: React.Dispatch<React.SetStateAction<string[]>>
  }
}) => {
  const { toggleModalWindow, setShowConfirmation, showConfirmation } =
    useContext(ModalWindowAndPopupContext)
  const { setTask } = useContext(CurrentTaskContext)
  const { setThereIsNewData } = useContext(ThereIsNewDataContext)

  const { Put, GetById } = useLocalstorage()

  const { selectMany, setSelectedIds } = selectManyFunctionality

  const openModalWindow = () => {
    setTask(task)
    toggleModalWindow()
  }

  const deleteTask = () => {
    setShowConfirmation({
      show: true,
      text: "delete the task",
      confirmed: false,
      id: task._id
    })
  }

  // Make a task done functionality

  useEffect(() => {
    if (
      !showConfirmation.show &&
      showConfirmation.text === "mark the task as completed" &&
      task._id === showConfirmation.id
    ) {
      const UpdatedTask = GetById(showConfirmation.id as string)
      if (UpdatedTask) {
        UpdatedTask.completed = true
        UpdatedTask.checklists = [
          ...UpdatedTask.checklists.map((checklist) => ({
            ...checklist,
            completed: true
          }))
        ]
        for (const checklist of UpdatedTask.checklists) {
          if (checklist.items.length > 0) {
            for (const item of checklist.items) {
              item.checked = true
            }
          }
        }
      }
      // вернуть состояние подтверждения в первоначальное
      setShowConfirmation({
        show: false,
        text: "",
        confirmed: false,
        id: ""
      })
      // Updating task in localStorage
      Put(UpdatedTask as Task)
    } else if (
      !showConfirmation.show &&
      showConfirmation.text === "mark the task as not completed" &&
      task._id === showConfirmation.id
    ) {
      const UpdatedTask = GetById(showConfirmation.id as string)
      if (UpdatedTask) {
        UpdatedTask.completed = false
        UpdatedTask.checklists = [
          ...UpdatedTask.checklists.map((checklist) => ({
            ...checklist,
            completed: false
          }))
        ]
        for (const checklist of UpdatedTask.checklists) {
          if (checklist.items.length > 0) {
            for (const item of checklist.items) {
              item.checked = false
            }
          }
        }
      }
      // вернуть состояние подтверждения в первоначальное
      setShowConfirmation({
        show: false,
        text: "",
        confirmed: false,
        id: ""
      })
      // Updating task in localStorage
      Put(UpdatedTask as Task)
    }
  }, [
    GetById,
    Put,
    setShowConfirmation,
    setTask,
    setThereIsNewData,
    showConfirmation.id,
    showConfirmation.show,
    showConfirmation.text,
    task._id
  ])

  const taskIsDone = (makeTaskDone: boolean) => {
    if (makeTaskDone) {
      setShowConfirmation({
        show: true,
        text: "mark the task as completed",
        confirmed: false,
        id: task._id
      })
    } else {
      setShowConfirmation({
        show: true,
        text: "mark the task as not completed",
        confirmed: false,
        id: task._id
      })
    }
  }

  // Multiple select functionality
  const addSelectedTaskId = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked)
      setSelectedIds((prevIds) => [...prevIds, task._id])
    if (!event.target.checked)
      setSelectedIds((prevIds) => [...prevIds.filter((id) => id !== task._id)])
  }

  return (
    <StyledTaskDiv
      background={
        isOverdue(task) && !task.completed
          ? LayoutColors.overdue
          : task.completed
          ? LayoutColors.completed
          : LayoutColors.countermain
      }>
      {selectMany ? (
        <StyledDarkCheckbox type="checkbox" onChange={addSelectedTaskId} />
      ) : null}
      <StyledEditIcon onClick={openModalWindow} />
      <StyledDeleteIcon onClick={deleteTask} />
      <StyledTaskTitle>{task.title}</StyledTaskTitle>
      {task.description ? (
        <StyledInfoIcon title="Task with description" />
      ) : null}
      <StyledCommentsIcon title="Comments in the task" />
      <StyledCommentsCounter>{task.comments.length}</StyledCommentsCounter>
      <StyledCheckedIcon title="Checklist items in the task" />
      <StyledCheckedCounter>
        {howManyChecklistItemsAreCheckedInTheTask(task)}/
        {howManyChecklistItemsAreThereInTheTask(task)}
      </StyledCheckedCounter>
      {task.completed ? (
        <StyledButton
          background={LayoutColors.cancel}
          color={LayoutColors.countermain}
          area="done"
          onClick={() => taskIsDone(false)}>
          Not done
        </StyledButton>
      ) : (
        <StyledButton
          background={LayoutColors.create}
          color={LayoutColors.countermain}
          area="done"
          onClick={() => taskIsDone(true)}>
          Done
        </StyledButton>
      )}
    </StyledTaskDiv>
  )
}
