import React, { useContext } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { StyledButton } from "../styled-components/styled-buttons"
import { StyledSimpleFormDiv } from "../styled-components/styled-divs"
import { DarkStyledInput } from "../styled-components/styled-inputs"
import { yupResolver } from "@hookform/resolvers/yup"
import { TaskTitleInput } from "../../types/input-types/input-types"
import { TaskTitleSchema } from "../../types/yup-schemas/yup-schemas"
import { StyledInputError } from "../styled-components/styled-text-related-elements"
import { useLocalstorage } from "../../hooks/use-local-storage"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import { LayoutColors, StandardButtonHeights } from "../../types/context-types"

export const AddTaskForm = () => {
  const { setShowPopup } = useContext(ModalWindowAndPopupContext)

  const { Post } = useLocalstorage()
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset
  } = useForm<TaskTitleInput>({
    resolver: yupResolver(TaskTitleSchema)
  })

  const onSubmit: SubmitHandler<TaskTitleInput> = (data) => {
    Post(data.Title)
    reset()
    setShowPopup({
      show: true,
      text: "New task successfully created.",
      type: "success"
    })
  }

  return (
    <StyledSimpleFormDiv
      area="add"
      background={LayoutColors.countermain}
      onSubmit={handleSubmit(onSubmit)}>
      <DarkStyledInput {...register("Title")} type="text" placeholder="Title" />
      <StyledInputError>{errors.Title?.message}</StyledInputError>
      <StyledButton
        type="submit"
        background={LayoutColors.create}
        color={LayoutColors.countermain}
        height={StandardButtonHeights.thick}>
        Add a task
      </StyledButton>
    </StyledSimpleFormDiv>
  )
}
