import React, { useContext } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { StyledButton } from "../styled-components/styled-buttons"
import { StyledSimpleFormDiv } from "../styled-components/styled-divs"
import { DarkStyledInput } from "../styled-components/styled-inputs"
import { yupResolver } from "@hookform/resolvers/yup"
import { TaskTitleInput } from "../../types/input-types/input-types"
import { TaskTitleSchema } from "../../types/yup-schemas/yup-schemas"
import { StyledInputError } from "../styled-components/styled-text-related-elements"
import { useLocalstorage } from "../../hooks/use-local-storage"
import { CurrentTaskContext } from "../../contexts/current-task"
import { Checklist } from "../../types/task-type"
import { getUniqueId } from "../../utilities/utility-functions"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import { LayoutColors, StandardButtonHeights } from "../../types/context-types"

export const AddChecklistForm = () => {
  const { setShowPopup } = useContext(ModalWindowAndPopupContext)
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Put } = useLocalstorage()
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset
  } = useForm<TaskTitleInput>({
    resolver: yupResolver(TaskTitleSchema)
  })

  const onSubmit: SubmitHandler<TaskTitleInput> = (data) => {
    const NewChecklist: Checklist = {
      _id: getUniqueId(task.checklists, "checklist"),
      name: data.Title,
      items: [],
      completed: false
    }
    const UpdatedTask = {
      ...task,
      checklists: [...task.checklists, NewChecklist]
    }
    // Updating data in storage
    Put(UpdatedTask)
    // Updating data in context
    setTask(UpdatedTask)
    reset()
    setShowPopup({
      show: true,
      text: "New checklist successfully created.",
      type: "success"
    })
  }

  return (
    <StyledSimpleFormDiv
      area="add"
      background={LayoutColors.countermain}
      onSubmit={handleSubmit(onSubmit)}>
      <DarkStyledInput
        {...register("Title")}
        type="text"
        placeholder="Checklist title"
      />
      <StyledInputError>{errors.Title?.message}</StyledInputError>
      <StyledButton
        type="submit"
        background={LayoutColors.create}
        color={LayoutColors.countermain}
        height={StandardButtonHeights.thick}>
        Add checklist
      </StyledButton>
    </StyledSimpleFormDiv>
  )
}
