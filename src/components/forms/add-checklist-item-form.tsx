import React, { useContext } from "react"
import { SubmitHandler, useFieldArray, useForm } from "react-hook-form"
import { StyledButton } from "../styled-components/styled-buttons"
import {
  ButtonsDiv,
  InputsDiv,
  StyledChecklistItemsFormDiv
} from "../styled-components/styled-divs"
import { DarkStyledInput } from "../styled-components/styled-inputs"
import { yupResolver } from "@hookform/resolvers/yup"
import { ChecklistItems } from "../../types/input-types/input-types"
import { ChecklistItemsSchema } from "../../types/yup-schemas/yup-schemas"
import { StyledInputError } from "../styled-components/styled-text-related-elements"
import { useLocalstorage } from "../../hooks/use-local-storage"
import { CurrentTaskContext } from "../../contexts/current-task"
import { Checklist, ChecklistItem } from "../../types/task-type"
import { getUniqueId } from "../../utilities/utility-functions"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import { LayoutColors, StandardButtonHeights } from "../../types/context-types"

export const AddChecklistItemForm = ({
  checklist
}: {
  checklist: Checklist
}) => {
  const { task, setTask } = useContext(CurrentTaskContext)
  const { setShowPopup } = useContext(ModalWindowAndPopupContext)

  const { Put } = useLocalstorage()
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
    reset
  } = useForm<ChecklistItems>({
    resolver: yupResolver(ChecklistItemsSchema)
  })
  const { fields, append, remove } = useFieldArray({
    name: "checklistItems",
    control
  })

  const onSubmit: SubmitHandler<ChecklistItems> = (data) => {
    const CurrentItems = [...checklist.items]
    const NewChecklistItems: ChecklistItem[] = data.checklistItems.map(
      (item) => {
        const GeneratedItem = {
          _id: getUniqueId([...CurrentItems], "checklistitem"),
          label: item.label,
          checked: false
        }
        // Push new items to the current items array to check for newly created ids
        CurrentItems.push(GeneratedItem)
        return GeneratedItem
      }
    )
    const UpdatedChecklist: Checklist = {
      ...checklist,
      completed: false,
      items: [...checklist.items, ...NewChecklistItems]
    }
    const UpdatedTask = {
      ...task,
      completed: false,
      checklists: [
        ...task.checklists.filter((list) => list._id !== checklist._id),
        UpdatedChecklist
      ]
    }
    // Updating data in storage
    Put(UpdatedTask)
    // Updating data in context
    setTask(UpdatedTask)
    reset()
    setShowPopup({
      show: true,
      text: "The checklist items were successfully added.",
      type: "success"
    })
  }

  return (
    <StyledChecklistItemsFormDiv onSubmit={handleSubmit(onSubmit)}>
      {fields.map((field, index) => {
        return (
          <InputsDiv key={field.id}>
            <DarkStyledInput
              {...register(`checklistItems.${index}.label` as const)}
            />
            <StyledButton
              background={LayoutColors.cancel}
              color={LayoutColors.countermain}
              height={StandardButtonHeights.thick}
              type="button"
              onClick={() => remove(index)}>
              Remove item
            </StyledButton>
            <StyledInputError>
              {errors?.checklistItems?.[index]?.label?.message}
            </StyledInputError>
          </InputsDiv>
        )
      })}
      <ButtonsDiv>
        <StyledButton
          background={LayoutColors.misc}
          color={LayoutColors.countermain}
          height={StandardButtonHeights.thick}
          type="button"
          onClick={() =>
            append({
              label: ""
            })
          }>
          Add item
        </StyledButton>
        {fields.length > 0 ? (
          <StyledButton
            type="submit"
            background={LayoutColors.create}
            color={LayoutColors.countermain}
            height={StandardButtonHeights.thick}>
            Save
          </StyledButton>
        ) : null}
      </ButtonsDiv>
    </StyledChecklistItemsFormDiv>
  )
}
