import React, { useContext } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { StyledButton } from "../styled-components/styled-buttons"
import { StyledSimpleFormDiv } from "../styled-components/styled-divs"
import { DarkStyledInput } from "../styled-components/styled-inputs"
import { yupResolver } from "@hookform/resolvers/yup"
import { CommentInput } from "../../types/input-types/input-types"
import { CommentInputSchema } from "../../types/yup-schemas/yup-schemas"
import { StyledInputError } from "../styled-components/styled-text-related-elements"
import { useLocalstorage } from "../../hooks/use-local-storage"
import { CurrentTaskContext } from "../../contexts/current-task"
import { Comment } from "../../types/task-type"
import { getUniqueId } from "../../utilities/utility-functions"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import { LayoutColors, StandardButtonHeights } from "../../types/context-types"

export const AddCommentForm = () => {
  const { setShowPopup } = useContext(ModalWindowAndPopupContext)
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Put } = useLocalstorage()
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset
  } = useForm<CommentInput>({
    resolver: yupResolver(CommentInputSchema)
  })

  const onSubmit: SubmitHandler<CommentInput> = (data) => {
    const NewComment: Comment = {
      _id: getUniqueId(task.comments, "comment"),
      body: data.body,
      created: new Date()
    }
    const UpdatedTask = {
      ...task,
      comments: [...task.comments, NewComment]
    }
    // Updating data in storage
    Put(UpdatedTask)
    // Updating data in context
    setTask(UpdatedTask)
    reset()
    setShowPopup({
      show: true,
      text: "The comment was added.",
      type: "success"
    })
  }

  return (
    <StyledSimpleFormDiv
      area="add"
      background={LayoutColors.countermain}
      onSubmit={handleSubmit(onSubmit)}>
      <DarkStyledInput
        {...register("body")}
        type="text"
        placeholder="Comment"
      />
      <StyledInputError>{errors.body?.message}</StyledInputError>
      <StyledButton
        type="submit"
        background={LayoutColors.create}
        color={LayoutColors.countermain}
        height={StandardButtonHeights.thick}>
        Comment!
      </StyledButton>
    </StyledSimpleFormDiv>
  )
}
