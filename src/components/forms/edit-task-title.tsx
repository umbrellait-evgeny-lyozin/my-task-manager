import React, { useContext } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { StyledButton } from "../styled-components/styled-buttons"
import { StyledSimpleFormDiv } from "../styled-components/styled-divs"
import { DarkStyledInput } from "../styled-components/styled-inputs"
import { yupResolver } from "@hookform/resolvers/yup"
import { TaskTitleInput } from "../../types/input-types/input-types"
import { TaskTitleSchema } from "../../types/yup-schemas/yup-schemas"
import { StyledInputError } from "../styled-components/styled-text-related-elements"
import { useLocalstorage } from "../../hooks/use-local-storage"
import { CurrentTaskContext } from "../../contexts/current-task"
import { LayoutColors, StandardButtonHeights } from "../../types/context-types"

export const EditTaskTitle = ({
  prevContent,
  setEditing
}: {
  setEditing: React.Dispatch<React.SetStateAction<boolean>>
  prevContent?: string
}) => {
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Put } = useLocalstorage()
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<TaskTitleInput>({
    resolver: yupResolver(TaskTitleSchema)
  })

  const onSubmit: SubmitHandler<TaskTitleInput> = (data) => {
    const UpdatedTask = {
      ...task,
      title: data.Title
    }
    // Updating data in storage
    Put(UpdatedTask)
    // Updating data in context
    setTask(UpdatedTask)
    // Return to actual title
    setEditing(false)
  }

  return (
    <StyledSimpleFormDiv
      area="title"
      background={LayoutColors.countermain}
      onSubmit={handleSubmit(onSubmit)}>
      <DarkStyledInput
        {...register("Title")}
        type="text"
        placeholder="Title"
        defaultValue={prevContent}
      />
      <StyledInputError>{errors.Title?.message}</StyledInputError>
      <StyledButton
        type="submit"
        background={LayoutColors.create}
        color={LayoutColors.countermain}
        height={StandardButtonHeights.thick}>
        Save
      </StyledButton>
    </StyledSimpleFormDiv>
  )
}
