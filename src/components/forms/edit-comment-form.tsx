import React, { useContext } from "react"
import { SubmitHandler, useForm } from "react-hook-form"
import { CurrentTaskContext } from "../../contexts/current-task"
import { useLocalstorage } from "../../hooks/use-local-storage"
import { CommentInput } from "../../types/input-types/input-types"
import { Comment } from "../../types/task-type"
import { CommentInputSchema } from "../../types/yup-schemas/yup-schemas"
import { StyledButton } from "../styled-components/styled-buttons"
import { StyledSimpleFormDiv } from "../styled-components/styled-divs"
import { DarkStyledInput } from "../styled-components/styled-inputs"
import { yupResolver } from "@hookform/resolvers/yup"
import { StyledInputError } from "../styled-components/styled-text-related-elements"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import { LayoutColors, StandardButtonHeights } from "../../types/context-types"

export const EditCommentForm = ({
  showEditForm,
  comment
}: {
  showEditForm: (bool: boolean) => void
  comment: Comment
}) => {
  const { setShowPopup } = useContext(ModalWindowAndPopupContext)
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Put } = useLocalstorage()
  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<CommentInput>({
    resolver: yupResolver(CommentInputSchema)
  })

  const onSubmit: SubmitHandler<CommentInput> = (data) => {
    if (data.body) {
      const commentToUpdate = task.comments.find(
        (acomment) => acomment._id === comment._id
      )
      if (commentToUpdate) commentToUpdate.body = data.body
      const updatedComments = [
        ...task.comments.filter((acomment) => acomment._id !== comment._id),
        commentToUpdate
      ]
      const UpdatedTask = {
        ...task,
        comments: [...(updatedComments as Comment[])]
      }
      // Updating data in storage
      Put(UpdatedTask)
      // Updating data in context
      setTask(UpdatedTask)
      showEditForm(false)
      setShowPopup({
        show: true,
        text: "The comment was updated.",
        type: "info"
      })
    } else {
      setShowPopup({
        show: true,
        text: "Trying to save a blank comment? Nope.",
        type: "failure"
      })
    }
  }

  return (
    <StyledSimpleFormDiv
      area="add"
      background={LayoutColors.countermain}
      onSubmit={handleSubmit(onSubmit)}>
      <DarkStyledInput
        {...register("body")}
        type="text"
        placeholder="Comment"
        defaultValue={comment.body}
      />
      <StyledInputError>{errors.body?.message}</StyledInputError>
      <StyledButton
        type="submit"
        background={LayoutColors.create}
        color={LayoutColors.countermain}
        height={StandardButtonHeights.thick}>
        Save
      </StyledButton>
      <StyledButton
        area="cancel"
        background={LayoutColors.cancel}
        color={LayoutColors.countermain}
        height={StandardButtonHeights.thick}
        onClick={() => showEditForm(false)}>
        Cancel
      </StyledButton>
    </StyledSimpleFormDiv>
  )
}
