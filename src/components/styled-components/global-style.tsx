import { createGlobalStyle } from "styled-components"
import { LayoutColors } from "../../types/context-types"

export const GlobalStyle = createGlobalStyle`
  * {
  margin: 0;
  padding: 0;
  font-family: "Comfortaa", cursive;
  font-weight: 100;
  color:  ${LayoutColors.main};
  word-break: break-word;
}

label {
  font-size: 1.5vw;
  color: ${LayoutColors.countermain};
}

h1 {
  font-size: 2.5vw;
}
h2 {
  font-size: 2vw;
}
h3 {
  font-size: 1.7vw;
}
p {
  font-size: 1.5vw;
}

@media (max-width: 768px) {
  * {
    font-weight: 400;
  }

  label {
    font-size: 3vw;
  }

  h1 {
    font-size: 4vw;
  }
  h2 {
    font-size: 3.5vw;
  }
  h3 {
    font-size: 3vw;
  }
  p {
    font-size: 2.5vw;
  }
}
@media (max-width: 480px) {
  label {
    font-size: 4vw;
  }

  h1 {
    font-size: 5vw;
  }
  h2 {
    font-size: 4.5vw;
  }
  h3 {
    font-size: 4vw;
  }
  p {
    font-size: 3.5vw;
  }
}

`
