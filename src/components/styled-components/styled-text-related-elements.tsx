import styled from "styled-components"
import { LayoutColors } from "../../types/context-types"

export const StyledTaskTitle = styled.h1`
  grid-area: title;
`
export const StyledInputError = styled.p`
  grid-area: error;
  width: 100%;
`
export const StyledH1Title = styled.h1`
  grid-area: title;
  background-color: white;
`
export const StyledCommentsCounter = styled.p`
  grid-area: commentscount;
`
export const StyledCheckedCounter = styled.p`
  grid-area: checkedcount;
`
export const StyledCommentParagraph = styled.p<{
  color?: string
}>`
  grid-area: comment;
  color: ${(props) => props.color || LayoutColors.main};
`

export const StyledCreatedDateHeader = styled.h3`
  grid-area: created;
  background-color: white;
`
export const StyledDueDateHeader = styled.h3<{ background: string }>`
  grid-area: due;
  background-color: ${(props) => props.background};
`

export const StyledH2Title = styled.h2`
  grid-area: title;
  background-color: white;
`

export const StyledH3Title = styled.h3`
  grid-area: title;
  background-color: white;
`
export const StyledChecklistParagraph = styled.p`
  grid-area: title;
`
