import styled from "styled-components"
import { LayoutColors } from "../../types/context-types"

export const StyledInput = styled.input`
  grid-area: input;
  width: 70%;
  height: 4.5vw;
  color: ${LayoutColors.countermain};
  background-color: ${LayoutColors.main};
  box-shadow: none;
  border-radius: 1vw;
  border-color: ${LayoutColors.countermain};
  border-width: 0.3vw;
  padding-left: 1vw;
  font-size: 2vw;
  border-style: solid;
  outline: none;
  &::placeholder {
    color: ${LayoutColors.countermain};
    opacity: 0.5;
  }
  @media (max-width: 768px) {
    font-size: 3vw;
  }
  @media (max-width: 480px) {
    font-size: 4vw;
  }
`
export const DarkStyledInput = styled(StyledInput)`
  color: ${LayoutColors.main};
  background-color: ${LayoutColors.countermain};
  border-color: ${LayoutColors.main};
  &::placeholder {
    color: ${LayoutColors.main};
    opacity: 0.5;
  }
  grid-area: input;
`
export const StyledSelect = styled.select`
  cursor: pointer;
  width: 100%;
  height: 2vw;
  background-color: ${LayoutColors.countermain};
  color: ${LayoutColors.main};
  border-radius: 1vw;
  border: none;
  font-size: 1.5vw;
  grid-area: sortinput;
  @media (max-width: 768px) {
    font-size: 2.5vw;
    height: 3vw;
  }
  @media (max-width: 480px) {
    font-size: 3.5vw;
    height: 4vw;
  }
`
export const StyledSortLabel = styled.label`
  grid-area: sortlabel;
`

export const StyledCompleteLabel = styled.label`
  grid-area: completelabel;
`

export const StyledCheckbox = styled.input`
  width: 2vw;
  height: 2vw;
  color: ${LayoutColors.main};
  border-radius: 1vw;
  border: none;
  font-size: 1.5vw;
  background-color: ${LayoutColors.countermain};
  -webkit-appearance: none;
  -moz-appearance: none;
  -o-appearance: none;
  appearance: none;
  cursor: pointer;
  grid-area: completeinput;
  &:checked {
    background-image: url("/icons/check.png");
    background-size: contain;
  }
  @media (max-width: 768px) {
    width: 3vw;
    height: 3vw;
  }
  @media (max-width: 480px) {
    width: 4vw;
    height: 4vw;
  }
`
export const StyledDarkCheckbox = styled(StyledCheckbox)`
  color: ${LayoutColors.countermain};
  background-color: ${LayoutColors.main};
  width: 1.5vw;
  height: 1.5vw;
  border-radius: 0.5vw;
  grid-area: checkbox;
`
export const StyledDateTimeInput = styled.input`
  background-color: ${LayoutColors.main};
  height: 3vw;
  grid-area: input;
  font-size: 1.5vw;
  color: ${LayoutColors.countermain};
  border: none;
  padding-left: 1vw;
  outline: none;
  cursor: pointer;
  &::placeholder {
    color: ${LayoutColors.main};
    opacity: 0.5;
  }
  @media (max-width: 768px) {
    font-size: 2.5vw;
  }
  @media (max-width: 480px) {
    font-size: 3.5vw;
  }
`
export const StyledTextArea = styled.textarea`
  width: 96%;
  height: 8vw;
  font-size: 1.5vw;
  outline: none;
  border: none;
  background-color: white;
  overflow: hidden;
  grid-area: textarea;
  padding: 2%;
  @media (max-width: 768px) {
    font-size: 2.5vw;
    height: 16vw;
  }
  @media (max-width: 480px) {
    font-size: 3.5vw;
    height: 23vw;
  }
`
