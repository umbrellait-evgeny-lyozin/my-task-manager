import styled from "styled-components"

export const StyledButton = styled.button<{
  background: string
  color: string
  height?: string
  area?: string
}>`
  background-color: ${(props) => props.background};
  color: ${(props) => props.color};
  height: ${(props) => props.height || "5vw"};
  grid-area: ${(props) => props.area || "button"};
  width: 9vw;
  border-radius: 1vw;
  border: none;
  cursor: pointer;
  font-size: 1.5vw;
  &:hover {
    opacity: 0.8;
  }
  @media (max-width: 768px) {
    font-size: 2vw;
    width: 11vw;
  }
  @media (max-width: 480px) {
    font-size: 2.5vw;
    width: 13vw;
    height: 7vw;
  }
`
