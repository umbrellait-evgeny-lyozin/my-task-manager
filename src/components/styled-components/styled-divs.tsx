import styled from "styled-components"
import { LayoutColors } from "../../types/context-types"

export const PaintedDiv = styled.div<{ background: string }>`
  background-color: ${(props) => props.background || "palevioletred"};
  border-radius: 1vw;
`
export const MainDiv = styled(PaintedDiv)`
  display: grid;
  grid-template-columns: 0.5fr 2fr 1fr 30% 1fr 2fr 0.5fr;
  grid-template-rows: 50px auto 50px;
  gap: 0px 0px;
  grid-template-areas:
    ". . . . . . ."
    ". . modal modal modal . ."
    ". . . . . . .";
  @media (max-width: 768px) {
    grid-template-columns: 10% 80% 10%;
    grid-template-rows: 50px auto 50px;
    gap: 0px 0px;
    grid-template-areas:
      ". . ."
      ". modal ."
      ". . .";
  }
  @media (max-width: 480px) {
    grid-template-columns: 1fr;
    grid-template-rows: 50px auto 50px;
    grid-template-areas:
      "."
      "modal"
      ".";
  }
`
export const TasksBoardDiv = styled(PaintedDiv)`
  grid-area: modal;
  -webkit-box-shadow: 0px 0px 15px 0px ${LayoutColors.main};
  -moz-box-shadow: 0px 0px 15px 0px ${LayoutColors.main};
  box-shadow: 0px 0px 15px 0px ${LayoutColors.main};
  display: grid;
  grid-template-columns: 0.2fr 2fr 0.1fr 1.5fr 0.2fr;
  grid-template-rows: 30px auto 80px auto 50px auto 30px;
  gap: 0px 0px;
  grid-template-areas:
    ". . . . ."
    ". input . sort ."
    ". . . . ."
    ". add add add ."
    ". . . . ."
    ". tasks tasks tasks ."
    ". . . . .";
`
export const ModalWindowDiv = styled(PaintedDiv)`
  -webkit-box-shadow: 0px 0px 0px 100vw ${LayoutColors.shadow};
  -moz-box-shadow: 0px 0px 0px 100vw ${LayoutColors.shadow};
  box-shadow: 0px 0px 0px 100vw ${LayoutColors.shadow};
  grid-area: modal;
  border-radius: 0px;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 0.1fr 4fr 1fr 0.1fr;
  grid-template-rows: 0.2fr 1fr 40px auto 40px auto 40px auto 40px auto 40px auto 0.2fr;
  gap: 0px 0px;
  grid-template-areas:
    ". . . ."
    ". . button ."
    ". . . ."
    ". title title ."
    ". . . ."
    ". dates dates ."
    ". . . ."
    ". description description ."
    ". . . ."
    ". checklists checklists ."
    ". . . ."
    ". comments comments ."
    ". . . .";
`
export const PopupDiv = styled(PaintedDiv)`
  width: 30vw;
  padding: 0.5vw;
  position: fixed;
  right: 1vw;
  top: 1vw;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 0.5fr 1fr 1fr 1fr 0.5fr;
  grid-template-rows: 0.5fr auto 1vw auto 0.5fr;
  gap: 0px 0px;
  grid-template-areas:
    ". . . . ."
    ". comment comment comment ."
    ". . . . ."
    ". . close . ."
    ". . . . .";
`

export const ConfirmationDiv = styled(PaintedDiv)`
  width: 30vw;
  padding: 0.5vw;
  position: fixed;
  right: 40%;
  top: 40%;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 0.5fr 1fr 1fr 1fr 0.5fr;
  grid-template-rows: 0.5fr auto 1vw auto 0.5fr;
  gap: 0px 0px;
  grid-template-areas:
    ". . . . ."
    ". comment comment comment ."
    ". . . . ."
    ". do . cancel ."
    ". . . . .";
  -webkit-box-shadow: 0px 0px 0px 100vw ${LayoutColors.shadow};
  -moz-box-shadow: 0px 0px 0px 100vw ${LayoutColors.shadow};
  box-shadow: 0px 0px 0px 100vw ${LayoutColors.shadow};
`

export const StyledSimpleFormDiv = styled.form<{
  background: string
  area: string
}>`
  background-color: ${(props) => props.background || "palevioletred"};
  border-radius: 1vw;
  grid-area: ${(props) => props.area};
  display: grid;
  grid-template-columns: 0.3fr 2fr 0.2fr 1fr 0.3fr;
  grid-template-rows: 20px 1fr auto auto 20px;
  gap: 0px 0px;
  grid-template-areas:
    ". . . . ."
    ". input . button ."
    ". error . . ."
    ". . . cancel ."
    ". . . . .";
`

export const StyledChecklistItemsFormDiv = styled.form`
  background-color: transparent;
  border-radius: 1vw;
  grid-area: add;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const StyledTaskDiv = styled(PaintedDiv)`
  margin-top: 2vw;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 0.3fr auto 0.3fr auto 0.3fr 0.5fr 1fr 0.5fr auto auto 0.1fr auto 0.1fr auto 0.3fr;
  grid-template-rows: 10px auto 30px auto 40px auto auto 10px;
  gap: 0px 0px;
  align-content: center;
  justify-content: center;
  justify-items: center;
  align-items: center;
  grid-template-areas:
    ". . . . . . . . . . . . . . ."
    ". checkbox . . . . . . . . . edit . delete ."
    ". . . . . . . . . . . . . . ."
    ". title title title title title title title title title title title title title ."
    ". . . . . . . . . . . . . . ."
    ". . . . . . done . . . . . . . ."
    ". info . commentsicon commentscount . done . checkedicon checkedcount . . . . ."
    ". . . . . . . . . . . . . . .";
`

export const StyledSortDiv = styled.div`
  grid-area: sort;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 0.5fr 0.5fr 0.5fr 0.5fr;
  grid-template-rows: 7fr 1fr 7fr;
  gap: 0px 0px;
  align-items: center;
  grid-template-areas:
    "sortlabel  sortinput sortinput sortinput"
    ". . . ."
    "completelabel completelabel completelabel completeinput";
  justify-items: end;
`
export const StyledTasksDiv = styled.div`
  grid-area: tasks;
  display: flex;
  flex-direction: column;
`
export const StyledTaskDatesDiv = styled.div`
  grid-area: dates;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: 1fr auto 1fr;
  gap: 5px 0px;
  grid-template-areas:
    "created . ."
    ". . button"
    "due . input";
  align-content: center;
  justify-items: end;
`

export const StyledTaskDescriptionDiv = styled.div`
  grid-area: description;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 1fr 40px 1fr;
  grid-template-rows: auto 30px auto 30px auto;
  gap: 0px 0px;
  grid-template-areas:
    "title title title"
    ". . ."
    "textarea textarea textarea"
    ". . ."
    "save . cancel";
  justify-items: center;
`

export const StyledTaskChecklistsDiv = styled.div`
  grid-area: checklists;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: auto 30px auto 30px auto;
  gap: 0px 0px;
  grid-template-areas:
    "title title"
    ". ."
    "add add"
    ". ."
    "checklists checklists";
`

export const StyledChecklistsDiv = styled.div`
  grid-area: checklists;
`
export const StyledChecklistDiv = styled.div<{ background: string }>`
  background-color: ${(props) => props.background};
  grid-area: checklist;
  display: grid;
  margin-bottom: 2vw;
  align-items: center;
  grid-auto-columns: 1fr;
  grid-template-columns: 3fr 0.3fr 1fr;
  grid-template-rows: auto 30px auto 30px auto;
  gap: 0px 0px;
  grid-template-areas:
    "title . delete"
    ". . ."
    "items items items"
    ". . ."
    "add add add";
`
export const StyledChecklistItemsDiv = styled.div`
  grid-area: items;
`
export const StyledChecklistItemDiv = styled.div`
  grid-area: item;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 0.3fr 0.3fr 3fr 0.5fr 1fr;
  grid-template-rows: auto;
  gap: 0px 0px;
  grid-template-areas: "checkbox . title . delete";
  align-items: center;
  margin-bottom: 10px;
  border-bottom: solid;
  border-bottom-style: dotted;
  border-width: 0.3vw;
  padding-bottom: 1vw;
`

export const StyledTaskCommentsDiv = styled.div`
  grid-area: comments;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: auto 30px auto 30px auto;
  gap: 0px 0px;
  grid-template-areas:
    "title title"
    ". ."
    "add add"
    ". ."
    "comments comments";
`
export const StyledCommentsDiv = styled.div`
  grid-area: comments;
  display: flex;
  flex-direction: column;
`

export const StyledCommentDiv = styled.div`
  grid-area: comment;
  display: grid;
  grid-auto-columns: 1fr;
  grid-template-columns: 1fr 1fr 0.5fr 1fr;
  grid-template-rows: 1fr 0.2fr 1fr 0.2fr auto;
  gap: 0px 0px;
  justify-items: end;
  grid-template-areas:
    "comment comment . delete"
    "comment comment . ."
    "comment comment . edit"
    ". . . ."
    "add add add add";
  margin-bottom: 2vw;
  padding: 1vw;
  background-color: white;
  align-content: center;
  justify-content: center;
  align-items: center;
`
export const InputsDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-bottom: 1vw;
  width: 100%;
  justify-content: space-between;
`

export const ButtonsDiv = styled.div`
  width: 100%;
    display: flex;
    justify-content: space-between;
}
`
