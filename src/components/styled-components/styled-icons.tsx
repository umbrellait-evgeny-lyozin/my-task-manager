import {
  BsCardChecklist,
  BsCardText,
  BsChatRightDots,
  BsPencil,
  BsXCircle
} from "react-icons/bs"
import styled from "styled-components"
import { LayoutColors } from "../../types/context-types"

export const StyledEditIcon = styled(BsPencil)`
  grid-area: edit;
  width: 3vw;
  height: 3vw;
  cursor: pointer;
  fill: ${LayoutColors.misc};
  &:hover {
    opacity: 0.8;
  }
  @media (max-width: 768px) {
    width: 4vw;
    height: 4vw;
  }
  @media (max-width: 480px) {
    width: 5vw;
    height: 5vw;
  }
`
export const StyledDeleteIcon = styled(BsXCircle)`
  grid-area: delete;
  width: 3vw;
  height: 3vw;
  cursor: pointer;
  fill: ${LayoutColors.danger};
  &:hover {
    opacity: 0.8;
  }
  @media (max-width: 768px) {
    width: 4vw;
    height: 4vw;
  }
  @media (max-width: 480px) {
    width: 5vw;
    height: 5vw;
  }
`

export const StyledInfoIcon = styled(BsCardText)`
  grid-area: info;
  width: 3vw;
  height: 3vw;
  @media (max-width: 768px) {
    width: 4vw;
    height: 4vw;
  }
  @media (max-width: 480px) {
    width: 5vw;
    height: 5vw;
  }
`
export const StyledCommentsIcon = styled(BsChatRightDots)`
  grid-area: commentsicon;
  width: 3vw;
  height: 3vw;
  @media (max-width: 768px) {
    width: 4vw;
    height: 4vw;
  }
  @media (max-width: 480px) {
    width: 5vw;
    height: 5vw;
  }
`

export const StyledCheckedIcon = styled(BsCardChecklist)`
  grid-area: checkedicon;
  width: 3vw;
  height: 3vw;
  @media (max-width: 768px) {
    width: 4vw;
    height: 4vw;
  }
  @media (max-width: 480px) {
    width: 5vw;
    height: 5vw;
  }
`
