import React, { useContext, useState } from "react"
import { CurrentTaskContext } from "../../contexts/current-task"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import { LayoutColors } from "../../types/context-types"
import { EditTaskTitle } from "../forms/edit-task-title"
import { StyledButton } from "../styled-components/styled-buttons"
import { ModalWindowDiv } from "../styled-components/styled-divs"
import { StyledH1Title } from "../styled-components/styled-text-related-elements"
import { TaskChecklists } from "./generic-task-form/task-checklists"
import { TaskComments } from "./generic-task-form/task-comments"
import { TaskDates } from "./generic-task-form/task-dates"
import { TaskDescription } from "./generic-task-form/task-description"

export const ModalWindow = () => {
  const { toggleModalWindow } = useContext(ModalWindowAndPopupContext)
  const { task } = useContext(CurrentTaskContext)

  const [editing, setEditing] = useState(false)

  const closeModalWindow = () => {
    toggleModalWindow()
  }
  return (
    <ModalWindowDiv background={LayoutColors.countermain}>
      <StyledButton
        background={LayoutColors.main}
        color={LayoutColors.countermain}
        onClick={closeModalWindow}>
        Close
      </StyledButton>
      {editing ? (
        <EditTaskTitle setEditing={setEditing} prevContent={task.title} />
      ) : (
        <StyledH1Title onClick={() => setEditing(true)}>
          {task.title}
        </StyledH1Title>
      )}
      <TaskDates created={task.created} due={task.due} />
      <TaskDescription description={task.description} />
      <TaskChecklists checklists={task.checklists} />
      <TaskComments comments={task.comments} />
    </ModalWindowDiv>
  )
}
