import React, { useContext, useState } from "react"
import { CurrentTaskContext } from "../../../contexts/current-task"
import { useLocalstorage } from "../../../hooks/use-local-storage"
import { LayoutColors } from "../../../types/context-types"
import { StyledButton } from "../../styled-components/styled-buttons"
import { StyledTaskDescriptionDiv } from "../../styled-components/styled-divs"
import { StyledTextArea } from "../../styled-components/styled-inputs"
import { StyledH1Title } from "../../styled-components/styled-text-related-elements"

export const TaskDescription = ({ description }: { description: string }) => {
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Put } = useLocalstorage()

  const [locked, setLocked] = useState(true)
  const [editedDescription, setEditedDescription] = useState(() =>
    task.description ? task.description : "Click here to edit description"
  )

  const lockTextArea = (bool: boolean) => {
    setLocked(bool)
  }

  const setDescription = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setEditedDescription(event.target.value)
    event.currentTarget.style.height = `10px`
    event.currentTarget.style.height = `${event.currentTarget.scrollHeight}px`
  }

  const saveDescription = () => {
    const UpdatedTask = {
      ...task,
      description: editedDescription
    }
    // Updating data in storage
    Put(UpdatedTask)
    // Updating data in context
    setTask(UpdatedTask)
    // Hiding buttons
    lockTextArea(true)
  }
  const cancelEdit = () => {
    lockTextArea(true)
    if (task.description) {
      setEditedDescription(task.description)
    } else {
      setEditedDescription("Click here to edit description")
    }
  }

  return (
    <StyledTaskDescriptionDiv>
      <StyledH1Title>Description</StyledH1Title>

      <StyledTextArea
        maxLength={300}
        onClick={() => lockTextArea(false)}
        readOnly={locked}
        value={editedDescription}
        onChange={setDescription}
      />
      {!locked ? (
        <>
          <StyledButton
            onClick={saveDescription}
            background={LayoutColors.create}
            color={LayoutColors.countermain}
            area="save">
            Save
          </StyledButton>
          <StyledButton
            area="cancel"
            onClick={cancelEdit}
            background={LayoutColors.main}
            color={LayoutColors.countermain}>
            Cancel
          </StyledButton>
        </>
      ) : null}
    </StyledTaskDescriptionDiv>
  )
}
