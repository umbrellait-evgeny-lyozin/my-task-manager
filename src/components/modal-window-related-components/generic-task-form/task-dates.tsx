import React, { useContext, useState } from "react"
import { CurrentTaskContext } from "../../../contexts/current-task"
import { ModalWindowAndPopupContext } from "../../../contexts/modal-window-and-popup"
import { useLocalstorage } from "../../../hooks/use-local-storage"
import {
  LayoutColors,
  StandardButtonHeights
} from "../../../types/context-types"
import { isOverdue } from "../../../utilities/utility-functions"
import { StyledButton } from "../../styled-components/styled-buttons"
import { StyledTaskDatesDiv } from "../../styled-components/styled-divs"
import { StyledDateTimeInput } from "../../styled-components/styled-inputs"
import {
  StyledCreatedDateHeader,
  StyledDueDateHeader
} from "../../styled-components/styled-text-related-elements"
export const TaskDates = ({
  created,
  due
}: {
  created: Date
  due: Date | null
}) => {
  const { setShowPopup } = useContext(ModalWindowAndPopupContext)
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Put } = useLocalstorage()

  const [dueDateString, setDueDateString] = useState("")

  const setDueDate = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDueDateString(event.currentTarget.value)
  }
  const updateDueDate = () => {
    if (dueDateString) {
      const UpdatedTask = {
        ...task,
        due: new Date(dueDateString)
      }
      // Updating data in storage
      Put(UpdatedTask)
      // Updating data in context
      setTask(UpdatedTask)
    } else {
      setShowPopup({
        show: true,
        text: "You have to pick a date first =-)",
        type: "failure"
      })
    }
  }
  return (
    <StyledTaskDatesDiv>
      <StyledCreatedDateHeader>
        Created: {created.toLocaleString("ru-RU")}
      </StyledCreatedDateHeader>
      <StyledDueDateHeader
        background={
          isOverdue(task) && !task.completed
            ? LayoutColors.overdue
            : task.completed
            ? LayoutColors.completed
            : LayoutColors.white
        }>
        Due: {due?.toLocaleString("ru-RU")}
      </StyledDueDateHeader>
      <StyledDateTimeInput
        onChange={setDueDate}
        type="datetime-local"
        required
      />
      <StyledButton
        onClick={updateDueDate}
        height={StandardButtonHeights.thick}
        background={LayoutColors.create}
        color={LayoutColors.countermain}>
        Set due date
      </StyledButton>
    </StyledTaskDatesDiv>
  )
}
