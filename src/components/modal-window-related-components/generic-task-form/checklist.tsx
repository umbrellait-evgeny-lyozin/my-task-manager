import React, { useContext, useEffect } from "react"
import { CurrentTaskContext } from "../../../contexts/current-task"
import { ModalWindowAndPopupContext } from "../../../contexts/modal-window-and-popup"
import { useLocalstorage } from "../../../hooks/use-local-storage"
import {
  LayoutColors,
  StandardButtonHeights
} from "../../../types/context-types"
import { Checklist, Task } from "../../../types/task-type"
import { AddChecklistItemForm } from "../../forms/add-checklist-item-form"
import { StyledButton } from "../../styled-components/styled-buttons"
import {
  StyledChecklistDiv,
  StyledChecklistItemsDiv
} from "../../styled-components/styled-divs"
import { StyledH2Title } from "../../styled-components/styled-text-related-elements"
import { SingleChecklistItem } from "./checklist-item"

export const SingleChecklist = ({ checklist }: { checklist: Checklist }) => {
  const { setShowConfirmation, showConfirmation, setShowPopup } = useContext(
    ModalWindowAndPopupContext
  )
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Delete, GetById, Put } = useLocalstorage()

  // To Delete checklist when the appropriate consent is aquaired
  useEffect(() => {
    if (
      !showConfirmation.show &&
      showConfirmation.text === "delete the checklist"
    ) {
      Delete(showConfirmation.id as string, "checklist")
      let UpdatedTask: Task = GetById(task._id) as Task
      // Check if the checklists are complete
      for (const somechecklist of (UpdatedTask as Task).checklists) {
        let isChecklistComplete = true
        if (somechecklist.items.length > 0) {
          for (const item of somechecklist.items) {
            if (!item.checked) isChecklistComplete = false
          }
        } else {
          isChecklistComplete = false
        }
        UpdatedTask = {
          ...UpdatedTask,
          checklists: (UpdatedTask as Task).checklists.map((singlechecklist) =>
            singlechecklist._id === somechecklist._id
              ? {
                  ...singlechecklist,
                  completed: isChecklistComplete
                }
              : singlechecklist
          )
        }
      }
      // check if the task is complete
      let isComplete = true
      if ((UpdatedTask as Task).checklists.length > 0) {
        for (const checklist of (UpdatedTask as Task).checklists) {
          if (checklist.items.length > 0 && !checklist.completed)
            isComplete = false
        }
      }
      UpdatedTask = {
        ...UpdatedTask,
        completed: isComplete
      }
      // Update data in storage
      Put(UpdatedTask)
      // Updating data in context
      setTask(UpdatedTask as Task)

      setShowPopup({
        show: true,
        text: "The checklist was successfully deleted.",
        type: "alert"
      })
    }
  }, [
    Delete,
    GetById,
    Put,
    setShowPopup,
    setTask,
    showConfirmation.id,
    showConfirmation.show,
    showConfirmation.text,
    task._id
  ])

  // To show a confirmation window
  const deleteChecklist = () => {
    setShowConfirmation({
      show: true,
      text: "delete the checklist",
      confirmed: false,
      id: checklist._id
    })
  }

  return (
    <StyledChecklistDiv
      background={
        checklist.completed ? LayoutColors.completed : LayoutColors.countermain
      }>
      <StyledH2Title>{checklist.name}</StyledH2Title>
      <StyledButton
        height={StandardButtonHeights.thick}
        area="delete"
        background={LayoutColors.danger}
        color={LayoutColors.countermain}
        onClick={deleteChecklist}>
        Delete checklist
      </StyledButton>
      <StyledChecklistItemsDiv>
        {checklist.items.map((item) => (
          <SingleChecklistItem item={item} key={item._id} />
        ))}
      </StyledChecklistItemsDiv>
      <AddChecklistItemForm checklist={checklist} />
    </StyledChecklistDiv>
  )
}
