import React from "react"
import { Checklist } from "../../../types/task-type"
import { AddChecklistForm } from "../../forms/add-checklist-form"
import {
  StyledChecklistsDiv,
  StyledTaskChecklistsDiv
} from "../../styled-components/styled-divs"
import { StyledH1Title } from "../../styled-components/styled-text-related-elements"
import { SingleChecklist } from "./checklist"

export const TaskChecklists = ({
  checklists
}: {
  checklists: Checklist[] | []
}) => {
  return (
    <StyledTaskChecklistsDiv>
      <StyledH1Title>Checklists</StyledH1Title>
      <AddChecklistForm />
      <StyledChecklistsDiv>
        {checklists.map((checklist) => (
          <SingleChecklist checklist={checklist} key={checklist._id} />
        ))}
      </StyledChecklistsDiv>
    </StyledTaskChecklistsDiv>
  )
}
