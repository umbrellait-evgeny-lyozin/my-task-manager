import React from "react"

export const ManipulateDueDateButton = ({
  formIsShownSetter,
  changeExistingDate,
}: {
  formIsShownSetter: React.Dispatch<React.SetStateAction<boolean>>
  changeExistingDate: boolean
}) => {
  const addDueDateButtonOnClickHandler = () => {
    formIsShownSetter(true)
  }

  const AddDueDateButton = () => (
    <button onClick={addDueDateButtonOnClickHandler}>Add due date</button>
  )
  const ChangeDueDateButton = () => (
    <button onClick={addDueDateButtonOnClickHandler}>Change due date</button>
  )

  return (
    <>{changeExistingDate ? <ChangeDueDateButton /> : <AddDueDateButton />}</>
  )
}
