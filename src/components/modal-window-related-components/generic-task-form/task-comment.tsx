import React, { useContext, useEffect, useState } from "react"
import { StyledCommentDiv } from "../../styled-components/styled-divs"
import {
  StyledDeleteIcon,
  StyledEditIcon
} from "../../styled-components/styled-icons"
import { StyledCommentParagraph } from "../../styled-components/styled-text-related-elements"
import { EditCommentForm } from "../../forms/edit-comment-form"
import { Comment, Task } from "../../../types/task-type"
import { ModalWindowAndPopupContext } from "../../../contexts/modal-window-and-popup"
import { CurrentTaskContext } from "../../../contexts/current-task"
import { useLocalstorage } from "../../../hooks/use-local-storage"

export const TaskComment = ({ comment }: { comment: Comment }) => {
  const [editing, setEditing] = useState(false)

  const showEditForm = (bool: boolean) => {
    setEditing(bool)
  }
  // Delete comment functionality
  const { setShowConfirmation, showConfirmation, setShowPopup } = useContext(
    ModalWindowAndPopupContext
  )
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Delete, GetById } = useLocalstorage()

  useEffect(() => {
    if (
      !showConfirmation.show &&
      showConfirmation.text === "delete the comment"
    ) {
      Delete(showConfirmation.id as string, "comment")
      const UpdatedTask = GetById(task._id)
      // Updating data in context
      setTask(UpdatedTask as Task)
      setShowPopup({
        show: true,
        text: "The comment was deleted.",
        type: "alert"
      })
    }
  }, [
    Delete,
    GetById,
    setShowPopup,
    setTask,
    showConfirmation.id,
    showConfirmation.show,
    showConfirmation.text,
    task._id
  ])

  const deleteComment = () => {
    setShowConfirmation({
      show: true,
      text: "delete the comment",
      confirmed: false,
      id: comment._id
    })
  }

  return (
    <StyledCommentDiv>
      <StyledDeleteIcon onClick={deleteComment} />
      <StyledCommentParagraph>{comment.body}</StyledCommentParagraph>
      {editing ? (
        <EditCommentForm comment={comment} showEditForm={showEditForm} />
      ) : (
        <StyledEditIcon onClick={() => showEditForm(true)} />
      )}
    </StyledCommentDiv>
  )
}
