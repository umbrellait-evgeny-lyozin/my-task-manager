import React from "react"
import {
  StyledTaskCommentsDiv,
  StyledCommentsDiv
} from "../../styled-components/styled-divs"
import { StyledH1Title } from "../../styled-components/styled-text-related-elements"
import { TaskComment } from "./task-comment"
import { Comment } from "../../../types/task-type"
import { AddCommentForm } from "../../forms/add-comment-form"

export const TaskComments = ({ comments }: { comments: Comment[] | [] }) => {
  return (
    <StyledTaskCommentsDiv>
      <StyledH1Title>Comments</StyledH1Title>
      <AddCommentForm />
      <StyledCommentsDiv>
        {comments.map((comment) => (
          <TaskComment comment={comment} key={comment._id} />
        ))}
      </StyledCommentsDiv>
    </StyledTaskCommentsDiv>
  )
}
