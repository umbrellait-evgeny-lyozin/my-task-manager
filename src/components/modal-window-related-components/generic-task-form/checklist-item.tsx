import React, { useContext, useEffect, useState } from "react"
import { CurrentTaskContext } from "../../../contexts/current-task"
import { ModalWindowAndPopupContext } from "../../../contexts/modal-window-and-popup"
import { useLocalstorage } from "../../../hooks/use-local-storage"
import { ChecklistItem, Task } from "../../../types/task-type"
import { StyledChecklistItemDiv } from "../../styled-components/styled-divs"
import { StyledDeleteIcon } from "../../styled-components/styled-icons"
import { StyledDarkCheckbox } from "../../styled-components/styled-inputs"
import { StyledChecklistParagraph } from "../../styled-components/styled-text-related-elements"

export const SingleChecklistItem = ({ item }: { item: ChecklistItem }) => {
  const { setShowConfirmation, showConfirmation } = useContext(
    ModalWindowAndPopupContext
  )
  const { task, setTask } = useContext(CurrentTaskContext)

  const { Delete, GetById, Put } = useLocalstorage()

  const [checked, setChecked] = useState(item.checked)

  // Delete checklist item functionality
  useEffect(() => {
    if (
      !showConfirmation.show &&
      showConfirmation.text === "delete the checklist item"
    ) {
      Delete(showConfirmation.id as string, "checklistitem")
      let UpdatedTask: Task = GetById(task._id) as Task
      // Check if the checklists are complete
      for (const somechecklist of (UpdatedTask as Task).checklists) {
        let isChecklistComplete = true
        if (somechecklist.items.length > 0) {
          for (const item of somechecklist.items) {
            if (!item.checked) isChecklistComplete = false
          }
        } else {
          isChecklistComplete = false
        }
        UpdatedTask = {
          ...UpdatedTask,
          checklists: (UpdatedTask as Task).checklists.map((singlechecklist) =>
            singlechecklist._id === somechecklist._id
              ? {
                  ...singlechecklist,
                  completed: isChecklistComplete
                }
              : singlechecklist
          )
        }
      }
      // check if the task is complete
      let isComplete = true
      if ((UpdatedTask as Task).checklists.length > 0) {
        for (const checklist of (UpdatedTask as Task).checklists) {
          if (checklist.items.length > 0 && !checklist.completed)
            isComplete = false
        }
      }
      UpdatedTask = {
        ...UpdatedTask,
        completed: isComplete
      }
      // Update data in storage
      Put(UpdatedTask)
      // Updating data in context
      setTask(UpdatedTask as Task)
    }
  }, [
    Delete,
    GetById,
    Put,
    setTask,
    showConfirmation.id,
    showConfirmation.show,
    showConfirmation.text,
    task._id
  ])

  const deleteChecklistItem = () => {
    setShowConfirmation({
      show: true,
      text: "delete the checklist item",
      confirmed: false,
      id: item._id
    })
  }

  // Complete checklist item functionality
  const completeItem = (event: React.ChangeEvent<HTMLInputElement>) => {
    let UpdatedTask: Task | {} = {}
    if (event.target.checked) {
      UpdatedTask = {
        ...task,
        checklists: task.checklists.map((checklist) => ({
          ...checklist,
          items: checklist.items.map((someitem) =>
            someitem._id === item._id
              ? {
                  ...item,
                  checked: true
                }
              : someitem
          )
        }))
      }
      setChecked(true)
    }
    if (!event.target.checked) {
      UpdatedTask = {
        ...task,
        checklists: task.checklists.map((checklist) => ({
          ...checklist,
          items: checklist.items.map((someitem) =>
            someitem._id === item._id
              ? {
                  ...item,
                  checked: false
                }
              : someitem
          )
        }))
      }
      setChecked(false)
    }
    // Check if the checklists are complete
    for (const somechecklist of (UpdatedTask as Task).checklists) {
      let isChecklistComplete = true
      if (somechecklist.items.length > 0) {
        for (const item of somechecklist.items) {
          if (!item.checked) isChecklistComplete = false
        }
      } else {
        isChecklistComplete = false
      }
      UpdatedTask = {
        ...UpdatedTask,
        checklists: (UpdatedTask as Task).checklists.map((singlechecklist) =>
          singlechecklist._id === somechecklist._id
            ? {
                ...singlechecklist,
                completed: isChecklistComplete
              }
            : singlechecklist
        )
      }
    }
    // check if the task is complete
    let isComplete = true
    if ((UpdatedTask as Task).checklists.length > 0) {
      for (const checklist of (UpdatedTask as Task).checklists) {
        if (checklist.items.length > 0 && !checklist.completed)
          isComplete = false
      }
    }
    UpdatedTask = {
      ...UpdatedTask,
      completed: isComplete
    }
    // Save updated task in localStorage
    Put(UpdatedTask as Task)
    // Updating data in context
    setTask(UpdatedTask as Task)
  }
  return (
    <StyledChecklistItemDiv>
      <StyledDarkCheckbox
        checked={checked}
        type="checkbox"
        onChange={completeItem}
      />
      <StyledChecklistParagraph
        style={checked ? { textDecoration: "line-through" } : {}}>
        {item.label}
      </StyledChecklistParagraph>
      <StyledDeleteIcon onClick={deleteChecklistItem} />
    </StyledChecklistItemDiv>
  )
}
