import React, { useContext, useEffect } from "react"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import {
  LayoutColors,
  PopupColors,
  StandardButtonHeights
} from "../../types/context-types"
import { StyledButton } from "../styled-components/styled-buttons"
import { PopupDiv } from "../styled-components/styled-divs"
import { StyledCommentParagraph } from "../styled-components/styled-text-related-elements"

export const InformationalPopup = () => {
  const { setShowPopup, showPopup } = useContext(ModalWindowAndPopupContext)

  const closePopup = () => {
    if (showPopup) {
      setShowPopup({
        show: false,
        text: "",
        type: "null"
      })
    }
  }

  // To close informational popup after 2 seconds
  useEffect(() => {
    if (showPopup) {
      setTimeout(() => {
        setShowPopup({
          show: false,
          text: "",
          type: "null"
        })
      }, 2000)
    }
  }, [setShowPopup, showPopup])

  return (
    <PopupDiv background={PopupColors[showPopup.type]}>
      <StyledCommentParagraph color={LayoutColors.countermain}>
        {showPopup.text}
      </StyledCommentParagraph>
      <StyledButton
        area="close"
        height={StandardButtonHeights.thin}
        background={LayoutColors.countermain}
        color={PopupColors[showPopup.type]}
        onClick={closePopup}>
        Close
      </StyledButton>
    </PopupDiv>
  )
}
