import React, { useContext } from "react"
import { ModalWindowAndPopupContext } from "../../contexts/modal-window-and-popup"
import {
  ActionsWhichNeedConfirmation,
  LayoutColors,
  StandardButtonHeights
} from "../../types/context-types"
import { StyledButton } from "../styled-components/styled-buttons"
import { ConfirmationDiv } from "../styled-components/styled-divs"
import { StyledCommentParagraph } from "../styled-components/styled-text-related-elements"

export const ConfirmationPopup = ({ what }: { what: string }) => {
  const { setShowConfirmation, showConfirmation } = useContext(
    ModalWindowAndPopupContext
  )

  const closePopup = (decision: boolean) => {
    if (decision) {
      setShowConfirmation({
        show: false,
        text: showConfirmation.text as ActionsWhichNeedConfirmation,
        confirmed: decision,
        id: showConfirmation.id
      })
    } else {
      setShowConfirmation({
        show: false,
        text: "",
        confirmed: decision,
        id: ""
      })
    }
  }
  return (
    <ConfirmationDiv background={LayoutColors.countermain}>
      <StyledCommentParagraph>
        Are you sure you want to {what}?
      </StyledCommentParagraph>
      <StyledButton
        area="do"
        height={StandardButtonHeights.thin}
        background={LayoutColors.main}
        color={LayoutColors.countermain}
        onClick={() => closePopup(true)}>
        Do it!
      </StyledButton>
      <StyledButton
        area="cancel"
        height={StandardButtonHeights.thin}
        background={LayoutColors.main}
        color={LayoutColors.countermain}
        onClick={() => closePopup(false)}>
        Cancel
      </StyledButton>
    </ConfirmationDiv>
  )
}
